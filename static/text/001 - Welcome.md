Title: Welcome
Date: April 21th 2015
Tags: Hello World

Hello everyone, this is the first serious blog post to show off Nomadic-Flask, a Python&Flask Markdown File based blog framework. This is still pretty much in heavy Work In Progress state as even all the features I would like to implement aren't set at this time and I'm still wondering how I will implement some of them, but well, I'm still on it!

## What's Nomadic-Flask?

Nomadic-Flask is an attempt to make an easy blog framework that wouldn't depend of any databases. I like Markdown, I use it on my own blog and I had the idea of this framework when trying to manage my posts locally in files in folders.

So, Nomadic-Flask is a web [Flask] server. It reads and processes text files stored in `static/text` in the order given by `static/index.json` as blogs posts. Thus, to write a blog post, you only need to write a new file, give it some meta details in the top lines and adding the filename on the top of `index.json`. Here's an example:

`sample.md` :

    Title: Tiny blog post example
    Date: Insert your date
    Tags: Insert, comma-separated, tags, here, The Game

    Here goes the text. Blah blah blah

`index.json`:

    [
    'index.md',
    'older_post.md',
    ...
    ]

And voilà, the next time you'll browse the main page, the blog will list your new post! Isn't that easy?

[Flask]: http://flask.pocoo.org/
