Title: Yay, some news!
Date: April 25th 2015
Tags: Update, Flask
Icon: /imgs/DukeWalrii-Sq.gif

Hello everyone, it's time for some news! I'm still under refactoring steps to clean the messy code part by part. For instance, all templates will take three arguments, the last, `content`, is used only in the template to generate the content the skeleton will read. Another things I added and fixed is the configuration parser, which will read the `config.json` file from the framework's root folder and will override its configuration with the options in the file. Thus, the user'll be able to, for instance, to change the blog's name, to open the server to outside access and not only `localhost` or even the Markdown's options.

So, the current state of the project is still messy, but the more I dig into, the more things I find or correct. I still find this quite fun and yet, I still have multiples problem to inspect. The main one will be the code structure and the second will be implementing tags in an effective manner.

Oh and one last thing : ![Duke Tape][duketape], it's one of my avatars, but it's also an example of picture parsing, given in the Extra Markdown options. I'm tempted to force rendering them as thumbnails and add a [lightbox]-like diaporama but it's a bit intrusive and it'll be quite hard to implement while dealing with Markdown. Well, it's something I can add in the *Why not?* list!


[duketape]: /imgs/DukeWalrii-Sq.gif
[lightbox]: http://lokeshdhakar.com/projects/lightbox2/
